﻿using Commons.Utils;
using Commons.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RazorPages.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IAgreementService _agreementService;
        public IList<AgreementsDTO> agreements { get; set; }

        public IndexModel(ILogger<IndexModel> logger, IAgreementService agreementService)
        {
            _logger = logger;
            _agreementService = agreementService;
        }

        public async Task OnGetAsync()
        {
            agreements = await _agreementService.GetAgreements();
        }
    }
}
