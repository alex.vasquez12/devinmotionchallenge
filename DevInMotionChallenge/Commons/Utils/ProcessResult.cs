﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.Utils
{
    public class ProcessResult
    {
        public dynamic Result { get; set; }
        public bool Response { get; set; }
        public string Message { get; set; }
        public ProcessResult()
        {
            this.Response = false;
            this.Message = "Internal server error";
        }
        public void SetResponse(bool response, string message = "")
        {
            this.Response = response;
            this.Message = message;
            if (!response && message == "") this.Message = "Internal server error";
        }
    }
}
