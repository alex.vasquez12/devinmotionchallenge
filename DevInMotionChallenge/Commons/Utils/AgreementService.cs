﻿using Commons.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Utils
{
    public interface IAgreementService
    {
        Task<List<AgreementsDTO>> GetAgreements();
    }
    public class AgreementService : IAgreementService
    {
        private HttpClient _httpClient;
        public AgreementService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task<List<AgreementsDTO>> GetAgreements()
        {
            List<AgreementsDTO> data = new List<AgreementsDTO>();
            try
            {
                HttpResponseMessage responseHttp = await _httpClient.GetAsync($"{_httpClient.BaseAddress.ToString()}/agreements");

                if (responseHttp.IsSuccessStatusCode)
                {
                    string stringResult = await responseHttp.Content.ReadAsStringAsync();
                    ProcessResult response = JsonConvert.DeserializeObject<ProcessResult>(stringResult);
                    if (response.Response)
                    {
                        string dataString = JsonConvert.SerializeObject(response.Result);
                        data = JsonConvert.DeserializeObject<List<AgreementsDTO>>(dataString);
                    }
                }
            }
            catch (Exception e)
            {

                throw;
            }
            
            
            return data;
        }
    }
}
