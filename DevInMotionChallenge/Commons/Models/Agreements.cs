﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.Models
{
    public class Agreements
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
    }
}
