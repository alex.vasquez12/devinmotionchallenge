﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.ViewModels
{
    public class AgreementsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
    }
}
