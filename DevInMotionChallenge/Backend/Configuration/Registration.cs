﻿using Backend.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Configuration
{
    public static class Registration
    {
        public static IServiceCollection AddRegistration(this IServiceCollection services)
        {
            AddServices(services);
            return services;
        }
        private static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddTransient<IAgreementsService, AgreementsService>();
            return services;
        }
    }
}
