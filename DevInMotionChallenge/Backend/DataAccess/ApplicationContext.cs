﻿using Commons.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.DataAccess
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
        : base(options)
        { }

        //Entities
        public virtual DbSet<Agreements> Agreements { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Agreements>().ToTable("Agreements");
            modelBuilder.Entity<Agreements>().HasKey(x => x.Id);
            modelBuilder.Entity<Agreements>().Property(x => x.Name).IsRequired();
            modelBuilder.Entity<Agreements>().Property(x => x.Amount).IsRequired();
        }
    }
}
