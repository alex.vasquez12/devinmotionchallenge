﻿using Backend.DataAccess;
using Commons.Utils;
using Commons.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    /// <summary>
    /// Interface AgreementsService
    /// </summary>
    public interface IAgreementsService
    {
        Task<ProcessResult> GetAgreements();
    }
    public class AgreementsService : IAgreementsService
    {
        private readonly ApplicationContext _context;
        public AgreementsService(ApplicationContext context)
        {
            _context = context;
        }
        public async Task<ProcessResult> GetAgreements()
        {
            ProcessResult response = new ProcessResult();
            try
            {
                var agreements = await _context.Agreements.ToListAsync();
                if (agreements.Count() == 0)
                {
                    response.SetResponse(false, "Not records");
                    return response;
                }
                response.Result = agreements.Select(a => new AgreementsDTO
                {
                    Id = a.Id,
                    Name = a.Name,
                    Description = a.Description,
                    Amount = a.Amount
                });
                response.SetResponse(true, "Done successfully!");
            }
            catch (Exception e)
            {
                response.Message = $"Error: {e.Message}";
            }
            return response;
        }
    }
}
