﻿using Backend.Services;
using Commons.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AgreementsController : ControllerBase
    {
        private readonly IAgreementsService _agreementsService;
        public AgreementsController(IAgreementsService agreementsService)
        {
            _agreementsService = agreementsService;
        }


        [HttpGet]
        public async Task<IActionResult> Get()
        {
            ProcessResult response = new ProcessResult();
            response = await _agreementsService.GetAgreements();
            if (!response.Response)
            {
                    return BadRequest(response);
            }
            return Ok(response);
        }
    }
}
