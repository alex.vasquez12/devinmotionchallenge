﻿using Commons.Utils;
using Commons.ViewModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace XamarinApp.Services
{
    public class AgreementsService : IDataStore<AgreementsDTO>
    {
        public Task<bool> AddItemAsync(AgreementsDTO item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<AgreementsDTO> GetItemAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<AgreementsDTO>> GetItemsAsync(bool forceRefresh = false)
        {
            HttpClient _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("http://10.0.2.2:62783/api");

            var service = new AgreementService(_httpClient);

            List<AgreementsDTO> data = await service.GetAgreements();

            return await Task.FromResult(data);
        }

        public Task<bool> UpdateItemAsync(AgreementsDTO item)
        {
            throw new NotImplementedException();
        }
    }
}
